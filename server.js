var express = require('express');
var app = express();
var _ = require('underscore');
var router = express.Router();
var pjson = require('./package.json');

var filteredProperties = _.pick(process.env, (value, key) => key.startsWith("IO_NOTEASY"));
var jsonProperties = _.map(_.pairs(filteredProperties), (pair) => {
  return {
    key: pair[0],
    value: pair[1]
  }
});
console.log(jsonProperties);

router.get('*', function (req, res) {
  res.sendFile('index.html', {
    root: 'build/'
  });
});

app.use(express.static('build'));


app.get('/config', function (req, res) {
  res.send(jsonProperties);
});

app.get('/ping', function (req, res) {
  res.send(200, "pong");
});

app.use('/', router);



app.get('/api/notes', function (req, res) {
  res.json([
    { title: 'Titulo 1', content: 'Content 1' },
    { title: 'Titulo 2', content: 'Content 2' },
    { title: 'Titulo 3', content: 'Content 3' },
    { title: 'Titulo 4', content: 'Content 4' },
    { title: 'Titulo 5', content: 'Content 5' }
  ]);
});


app.listen(4000, function () {

});
