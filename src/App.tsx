import React, { Component } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import FullLayout from "./layout/FullLayout";
import { Provider } from "react-redux";
import { createStore } from "redux";
import NotificationsSystem from "reapop";
const theme = require("reapop-theme-wybo");
import { rootReducer } from "./state/index";

let store = createStore(rootReducer);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <NotificationsSystem theme={theme} />
        <div className="App">
          <FullLayout />
        </div>
      </Provider>
    );
  }
}

export default App;
