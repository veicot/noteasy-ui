import { Note } from "../state/notes/Types";
import superagent, { SuperAgentStatic } from 'superagent';
import { Dispatch } from "redux";
import { fetchNotesSuccess, fetchNotesError, fetchNotes, fetchNote, fetchNoteError, fetchNoteSuccess } from "../state/notes/Actions";

const type = 'application/json';

export default class NoteService {

    getAllNotes(page: number, size: number, filter: string) {
        console.log('jajajajaj')
        return (dispatch: Dispatch) => {
            console.log('JEJEJEJEJE')
            dispatch(fetchNotes(page, size, filter));
            superagent
                .get('http://localhost:4000/api/notes')
                .set('content-type', type)
                .set('accept', type)
                .end((err, res) => {
                    if (err) {
                        dispatch(fetchNotesError(err));
                    } else {
                        dispatch(fetchNotesSuccess(res.body));
                    }
                });
        }
    }

    getNote(id: number) {
        console.log('asdasdsadasdasd')
        return (dispatch: Dispatch) => {
            console.log('JEJEJEJEJE')
            dispatch(fetchNote(id));
            superagent
                .get('http://localhost:4000/api/notes/' + id)
                .set('content-type', type)
                .set('accept', type)
                .end((err, res) => {
                    if (err) {
                        dispatch(fetchNoteError(err));
                    } else {
                        dispatch(fetchNoteSuccess(res.body));
                    }
                });
        }
    }



}