import React from 'react';
import TitleBar from '../components/atoms/TitleBar';
import NotesList from '../components/molecules/NotesList';
import { Container, Row, Col } from 'react-bootstrap';
import NoteEditor from '../components/molecules/NoteEditor';
import NotePreview from '../components/molecules/NotePreview';
import { connect } from 'react-redux';
import { AppState } from '../state';
import { Dispatch } from 'redux';
import NoteService from '../services/NoteService';
import { Note } from '../state/notes/Types';


const noteService = new NoteService();


interface FullLayoutProps {
    getAllNotes: any,
    selected?: Note,
    loading: boolean
}

class FullLayout extends React.Component<FullLayoutProps> {


    componentDidMount() {
        this.props.getAllNotes(0, 10, "");
    }

    render() {
        return (<div>
            <TitleBar></TitleBar>
            <Row>
                <Col xs={2}><NotesList></NotesList></Col>
                <Col>
                    <NoteEditor note={this.props.selected} loading={this.props.loading}></NoteEditor>
                    <NotePreview note={this.props.selected} loading={this.props.loading}></NotePreview>
                </Col>
            </Row>

        </div>);
    }
}


const mapStateToProps = (state: AppState) => ({
    notes: state.notes.notes,
    loading: state.notes.loading,
    selected: state.notes.selected
});

const mapDispatchToProps = (dispatch: Dispatch, props: any) => ({
    getAllNotes: (a: number, b: number, c: string) => noteService.getAllNotes(a, b, c)(dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(FullLayout);

