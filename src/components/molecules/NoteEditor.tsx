import React from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';
import { Note } from '../../state/notes/Types';
import Loading from '../atoms/Loading';


interface NoteEditorProps {
    note?: Note,
    loading: boolean
}

const NoteEditor = (props: NoteEditorProps) => {
    return (
        <Card body>
            <Loading loading={props.loading} />
            {props.note && props.note.content}
        </Card>
    );
}

export default NoteEditor;
