import React from 'react';
import { ListGroup } from 'react-bootstrap';
import NoteItem from '../atoms/NoteItem';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../state';
import { fetchNote } from '../../state/notes/Actions';
import { Note, NoteAction, FetchNoteAction } from '../../state/notes/Types';
import NoteService from '../../services/NoteService';


const noteService = new NoteService();

interface NoteListProps {
    notes: Note[],
    loading: boolean,
    selected: Note,
    select(id: number): any
}

const isActive = (note: Note, selected?: Note) => selected != undefined && note.title == selected.title;

const NotesList = (props: NoteListProps) => {

    const items = props.notes.map(n => <NoteItem key={n.title} note={n} active={isActive(n, props.selected)} select={() => props.select(n.id)}></NoteItem>)

    return (
        <div>
            <ListGroup>
                {items}
            </ListGroup>
        </div>
    );
}


const mapStateToProps = (state: AppState) => ({
    notes: state.notes.notes,
    loading: state.notes.loading,
    selected: state.notes.selected!
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
    select: (id: number) => noteService.getNote(id)(dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(NotesList);
