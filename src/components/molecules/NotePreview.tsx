import React from 'react';
import { Card, Spinner } from 'react-bootstrap';
import { Note } from '../../state/notes/Types';
import Loading from '../atoms/Loading';


interface NotePreviewProps {
    note?: Note,
    loading: boolean
}

const NotePreview = (props: NotePreviewProps) => {

    return (
        <Card body>
            <Loading loading={props.loading} />
            {props.note && props.note.content}
        </Card>
    );
}

export default NotePreview;
