import React from "react";
import { Spinner } from "react-bootstrap";

interface LoadingProps {
  loading: boolean;
}

const Loading = (props: LoadingProps) => {
  return props.loading ? <Spinner animation="grow" /> : <div />;
};

export default Loading;
