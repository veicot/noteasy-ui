import React from "react";
import { ListGroup } from "react-bootstrap";
import { Note } from "../../state/notes/Types";

interface NoteItemProps {
  note: Note;
  active: boolean;
  select(): void;
}

const NoteItem = (props: NoteItemProps) => {
  return (
    <ListGroup.Item action active={props.active} onClick={() => props.select()}>
      {props.note.title}
    </ListGroup.Item>
  );
};

export default NoteItem;
