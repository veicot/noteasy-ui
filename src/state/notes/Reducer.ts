import {
    Note,
    NotesState,
    NoteAction,
    SAVE_NOTE,
    SAVE_NOTE_SUCCESS,
    FETCH_NOTES,
    FETCH_NOTES_SUCCESS,
    FETCH_NOTES_ERROR,
    SAVE_NOTE_ERROR,
    SELECT_NOTE,
    FETCH_NOTE,
    FETCH_NOTE_SUCCESS,
    FETCH_NOTE_ERROR
} from "./Types";
import NoteService from "../../services/NoteService";


const initilState: NotesState = {
    page: 0,
    size: 10,
    loading: false,
    notes: [],
    selected: undefined
};


const noteService = new NoteService();

export const notesReducer = (state = initilState, action: NoteAction) => {

    switch (action.type) {
        // FETCH Notes
        case FETCH_NOTES:
            return { ...state, loading: true, notes: [] }
        case FETCH_NOTES_SUCCESS:
            return { ...state, loading: false, notes: action.payload }
        case FETCH_NOTES_ERROR:
            return { ...state, loading: false, notes: [] }


        // FETCH Single Note
        case FETCH_NOTE:
            return { ...state, loading: true, selected: undefined }
        case FETCH_NOTE_SUCCESS:
            return { ...state, loading: false, selected: action.payload }
        case FETCH_NOTE_ERROR:
            return { ...state, loading: false, selected: undefined }


        // SELECT Note
        case SELECT_NOTE:
            return { ...state, selected: action.payload }

        // SAVE Note
        case SAVE_NOTE:
            return { ...state, loading: true }
        case SAVE_NOTE_SUCCESS:
            return { ...state, loading: false }
        case SAVE_NOTE_ERROR:
            return { ...state, loading: false }

        default:
            return state
    }

}