
// MODELS
export interface FetchNotes {
    page?: number;
    size?: number;
    filter: string;
}

export interface Note {
    id: number,
    title: string,
    content: string
}


export interface NotesState {
    loading: boolean
    page: number;
    size: number;
    notes: Note[];
    selected?: Note
}


// ACTIONS

export const FETCH_NOTES = "FETCH_NOTES";
export const FETCH_NOTES_SUCCESS = "FETCH_NOTES_SUCCESS";
export const FETCH_NOTES_ERROR = "FETCH_NOTES_ERROR";

export const FETCH_NOTE = "FETCH_NOTE";
export const FETCH_NOTE_SUCCESS = "FETCH_NOTE_SUCCESS";
export const FETCH_NOTE_ERROR = "FETCH_NOTE_ERROR";

export const SAVE_NOTE = "SAVE_NOTE";
export const SAVE_NOTE_SUCCESS = "SAVE_NOTE_SUCCESS";
export const SAVE_NOTE_ERROR = "SAVE_NOTE_ERROR";


export const DELETE_NOTE = "DELETE_NOTE";
export const DELETE_NOTE_SUCCESS = "DELETE_NOTE_SUCCESS";
export const DELETE_NOTE_ERROR = "DELETE_NOTE_ERROR";

export const SELECT_NOTE = "SELECT_NOTE";



export interface FetchNotesAction {
    type: typeof FETCH_NOTES;
    payload: FetchNotes;
}

export interface FetchNotesSuccessAction {
    type: typeof FETCH_NOTES_SUCCESS;
    payload: Note[];
}

export interface FetchNotesErrorAction {
    type: typeof FETCH_NOTES_ERROR;
    payload: string;
}

// 

export interface FetchNoteAction {
    type: typeof FETCH_NOTE;
    payload: number;
}

export interface FetchNoteSuccessAction {
    type: typeof FETCH_NOTE_SUCCESS;
    payload: Note;
}

export interface FetchNoteErrorAction {
    type: typeof FETCH_NOTE_ERROR;
    payload: string;
}


//

export interface SaveNoteAction {
    type: typeof SAVE_NOTE;
    payload: Note;
}

export interface SaveNoteSuccessAction {
    type: typeof SAVE_NOTE_SUCCESS;
    payload: number;
}

export interface SaveNoteErrorAction {
    type: typeof SAVE_NOTE_ERROR;
    payload: string;
}

export interface DeleteNoteAction {
    type: typeof DELETE_NOTE;
    payload: number;
}


// SELECT

export interface SelectNoteAction {
    type: typeof SELECT_NOTE;
    payload: Note;
}




export type NoteAction =
    FetchNotesAction | FetchNotesSuccessAction | FetchNotesErrorAction |
    FetchNoteAction | FetchNoteSuccessAction | FetchNoteErrorAction |
    SaveNoteAction | SaveNoteSuccessAction | SaveNoteErrorAction |
    DeleteNoteAction |
    SaveNoteSuccessAction |
    SelectNoteAction;