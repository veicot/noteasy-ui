import {
    Note,
    FetchNotesAction,
    FETCH_NOTES,
    FetchNotesSuccessAction,
    FETCH_NOTES_SUCCESS,
    FetchNotesErrorAction,
    FETCH_NOTES_ERROR,
    SelectNoteAction,
    SELECT_NOTE,
    FetchNoteAction,
    FetchNoteSuccessAction,
    FetchNoteErrorAction,
    FETCH_NOTE,
    FETCH_NOTE_SUCCESS,
    FETCH_NOTE_ERROR
} from "./Types";


export const fetchNotes = (page: number, size: number, filter: string): FetchNotesAction => ({
    type: FETCH_NOTES,
    payload: { page: page, size: size, filter: filter }
});


export const fetchNotesSuccess = (notes: Note[]): FetchNotesSuccessAction => ({
    type: FETCH_NOTES_SUCCESS,
    payload: notes
})

export const fetchNotesError = (error: string): FetchNotesErrorAction => ({
    type: FETCH_NOTES_ERROR,
    payload: error
})

export const selectNote = (note: Note): SelectNoteAction => ({
    type: SELECT_NOTE,
    payload: note
})


export const fetchNote = (id: number): FetchNoteAction => ({
    type: FETCH_NOTE,
    payload: id
});


export const fetchNoteSuccess = (note: Note): FetchNoteSuccessAction => ({
    type: FETCH_NOTE_SUCCESS,
    payload: note
})

export const fetchNoteError = (error: string): FetchNoteErrorAction => ({
    type: FETCH_NOTE_ERROR,
    payload: error
})