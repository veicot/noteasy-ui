import { notesReducer } from './notes/Reducer';

import { combineReducers } from 'redux';


export const rootReducer = combineReducers({
    notes: notesReducer
})

export type AppState = ReturnType<typeof rootReducer>