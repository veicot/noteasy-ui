FROM node:10

WORKDIR /usr/src/app

COPY package*.json ./

COPY yarn.lock ./

COPY node_modules/ ./node_modules

COPY build/ ./build

COPY server.js ./

EXPOSE 3000

CMD ["node","server.js"]
