var express = require("express");
var app = express();
var _ = require("underscore");
var router = express.Router();
var cors = require("cors");
var pjson = require("./package.json");

var filteredProperties = _.pick(process.env, (value, key) =>
  key.startsWith("IO_NOTEASY")
);
var jsonProperties = _.map(_.pairs(filteredProperties), pair => {
  return {
    key: pair[0],
    value: pair[1]
  };
});
console.log(jsonProperties);

app.use(cors());

var db = [
  { id: 1, title: "Titulo 1", content: "Content 1" },
  { id: 2, title: "Titulo 2", content: "Content 2" },
  { id: 3, title: "Titulo 3", content: "Content 3" },
  { id: 4, title: "Titulo 4", content: "Content 4" },
  { id: 5, title: "Titulo 5", content: "Content 5" }
];

app.get("/api/notes", function(req, res) {
  res.json(db);
});

app.get("/api/notes/:id", function(req, res) {
  setTimeout(() => res.json(db.filter(n => n.id == req.params.id)[0]), 1500);
});

app.listen(4000, function() {});
